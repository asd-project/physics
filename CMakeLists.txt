#--------------------------------------------------------
#	asd physics
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.3)

project(physics VERSION 0.1)

#--------------------------------------------------------

include(bootstrap.cmake)

#--------------------------------------------------------

module(STATIC)
	domain(physics)

    group(include Headers)
    files(
        physics.h
        physical_layer.h
        plane_object.h
    )

    group(src Sources)
    files(physics.cpp)
endmodule()

#--------------------------------------------------------
