//---------------------------------------------------------------------------

#include <physics/physics.h>
#include <physics/plane_object.h>
#include <physics/physical_layer.h>

//---------------------------------------------------------------------------

namespace asd
{
    void motion_state::getWorldTransform(btTransform & trans) const {
        auto p = object.position();
        auto r = object.rotation();
        trans.setOrigin({p.x, p.y, p.z});
        trans.setRotation({r.x, r.y, r.z});
    }
    
    void motion_state::setWorldTransform(const btTransform & trans) {
    
        object.set_rotation(from_bullet(trans.getRotation()));
        object.set_position(from_bullet(trans.getOrigin()));
    }
    
    void physical::setMass(space::real mass) {
        btVector3 inertia;
        _rigidBody->getCollisionShape()->calculateLocalInertia(mass, inertia);
        _rigidBody->setMassProps(mass, inertia);
    }
    
    void physical::setLinearVelocity(const velocity & v) {
        _rigidBody->setLinearVelocity({v.x, v.y, v.z});
        _rigidBody->activate();
    }
    
    velocity physical::getLinearVelocity() {
        return from_bullet(_rigidBody->getLinearVelocity());
    }
    
    void physical_layer::update(btDynamicsWorld * world, btScalar timeStep) {
        //auto * w = static_cast<PhysicalLayer *>(world->getWorldUserInfo());
        
        int num = world->getDispatcher()->getNumManifolds();
        contact_info info;
        
        for(int i = 0; i < num; i++) {
            auto * manifold = world->getDispatcher()->getManifoldByIndexInternal(i);
            
            if(manifold->getNumContacts() == 0) {
                continue;
            }
            
            auto * a = static_cast<physical *>(manifold->getBody0() != nullptr ? manifold->getBody0()->getUserPointer() : nullptr);
            auto * b = static_cast<physical *>(manifold->getBody1() != nullptr ? manifold->getBody1()->getUserPointer() : nullptr);
            
            for(int j = 0; j < manifold->getNumContacts(); j++) {
                auto & pt = manifold->getContactPoint(j);
                
                if(pt.getDistance() <= 0.0) {
                    info.force = pt.m_appliedImpulse / timeStep;
                    info.normal = from_bullet(pt.m_normalWorldOnB);
                    
                    if(a != nullptr) {
                        info.pos = from_bullet(pt.getPositionWorldOnA());
                        a->contact_with(b, info);
                    }
                    
                    if(b != nullptr) {
                        info.pos = from_bullet(pt.getPositionWorldOnB());
                        b->contact_with(a, info);
                    }
                }
            }
        }
    }
}

//---------------------------------------------------------------------------
