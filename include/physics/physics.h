//---------------------------------------------------------------------------

#pragma once

#ifndef PHYSICS_H
#define PHYSICS_H

//---------------------------------------------------------------------------

#include <stdexcept>

#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>

#include <physics/physical_layer.h>

//---------------------------------------------------------------------------

namespace asd
{
    static inline space::vector from_bullet(const btVector3 & in) {
        space::vector out;
        out.set(in.x(), in.y(), in.z(), 1.0);

        return out;
    }

    static inline space::quaternion from_bullet(const btQuaternion & in) {
        space::quaternion out;
        out.set(in);

        return out;
    }

    static inline btVector3 to_bullet(const space::vector & in) {
        btVector3 out;
        out.setValue(in.x, in.y, in.z);
        return out;
    }

    static inline btQuaternion to_bullet(const space::quaternion & in) {
        btQuaternion out;
        out.setValue(in.x, in.y, in.z);

        return out;
    }

    struct contact_sensor_callback : public btCollisionWorld::ContactResultCallback
    {
        using base_type = btCollisionWorld::ContactResultCallback;

        contact_sensor_callback(btRigidBody * body, array_list<btVector3> & points)
            : base_type(), body(body), points(points) {
        }

        btRigidBody * body;
        array_list<btVector3> & points;

        virtual bool needsCollision(btBroadphaseProxy * proxy) const {
            if (!base_type::needsCollision(proxy)) {
                return false;
            }

            return body->checkCollideWithOverride(static_cast<btCollisionObject *>(proxy->m_clientObject));
        }

        // WHO THE HELL HAS MADE SUCH SIGNATURE?!!
        virtual btScalar addSingleResult(
            btManifoldPoint & cp, const btCollisionObjectWrapper * objA, int partA, int indexA, const btCollisionObjectWrapper * objB, int partB, int indexB) override {
            if (body == objA->m_collisionObject) {
                points.push_back(cp.m_localPointA);
            } else if (body == objB->m_collisionObject) {
                points.push_back(cp.m_localPointB);
            } else {
                throw std::runtime_error("Body does not match either collision object");
            }

            return 0;
        }
    };

    class physical;

    class motion_state : public btMotionState
    {
    public:
        motion_state(space::spatial & object)
            : object(object) {
        }

        virtual ~motion_state() {
        }

        virtual api(physics) void getWorldTransform(btTransform & trans) const override;
        virtual api(physics) void setWorldTransform(const btTransform & trans) override;

        space::spatial & object;
    };

    using velocity = space::vector;

    class physical
    {
    public:
        physical(space::spatial & object, physical_layer & world, btCollisionShape & shape, space::real mass = 0.0f) :
            _world(world),
            _motionState(std::make_unique<motion_state>(object))
        {
            btVector3 inertia;
            shape.calculateLocalInertia(mass, inertia);

            btRigidBody::btRigidBodyConstructionInfo info(mass, _motionState.get(), &shape, inertia);
            info.m_restitution = 0.66f;
            info.m_friction = 0.8f;

            _rigidBody = std::make_unique<btRigidBody>(info);
            _rigidBody->setUserPointer(this);
            _world.addRigidBody(_rigidBody.get());
        }

        virtual ~physical() {
            _world.removeRigidBody(_rigidBody.get());
        }

        btRigidBody * body() const {
            return _rigidBody.get();
        }

        virtual api(physics) void setMass(space::real mass);
        virtual api(physics) void setLinearVelocity(const velocity & v);

        api(physics)
        velocity getLinearVelocity();

        virtual void contact_with(physical * object, const contact_info & info) {}

    protected:
        physical_layer & _world;

        std::unique_ptr<btRigidBody> _rigidBody;
        std::unique_ptr<btMotionState> _motionState;
    };
}

//---------------------------------------------------------------------------
#endif
