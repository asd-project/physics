//---------------------------------------------------------------------------

#pragma once

#ifndef PHYSICAL_LAYER_H
#define PHYSICAL_LAYER_H

//---------------------------------------------------------------------------

#include <container/array_list.h>

#include <space/spatial.h>

#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>

//---------------------------------------------------------------------------

namespace asd
{
	class physical;

	struct alignas(alignof(space::vector)) contact_info
	{
        space::vector normal;
        space::vector pos;
        space::real force;
	};

	class physical_layer : public btDiscreteDynamicsWorld
	{
		deny_copy(physical_layer);

	public:
		physical_layer(std::unique_ptr<btDispatcher> && dispatcher, std::unique_ptr<btBroadphaseInterface> && broadphase, std::unique_ptr<btConstraintSolver> && solver, std::unique_ptr<btCollisionConfiguration> && config) :
            btDiscreteDynamicsWorld(dispatcher.get(), broadphase.get(), solver.get(), config.get()),
			dispatcher(std::move(dispatcher)),
			broadphase(std::move(broadphase)),
			solver(std::move(solver)),
			config(std::move(config))
		{
			setInternalTickCallback(physical_layer::update, this);
			setGravity({0.0f, -10.0f, 0.0f});
		}

	protected:
		//using btDiscreteDynamicsWorld::operator new;
		//using btDiscreteDynamicsWorld::operator delete;
		//using btDiscreteDynamicsWorld::operator new[];
		//using btDiscreteDynamicsWorld::operator delete[];

		static api(physics) void update(btDynamicsWorld * world, btScalar timeStep);

		std::unique_ptr<btDispatcher> dispatcher;
        std::unique_ptr<btBroadphaseInterface> broadphase;
        std::unique_ptr<btConstraintSolver> solver;
        std::unique_ptr<btCollisionConfiguration> config;
	};
}

//---------------------------------------------------------------------------
#endif
