//---------------------------------------------------------------------------

#pragma once

#ifndef PLANE_OBJECT_H
#define PLANE_OBJECT_H

//---------------------------------------------------------------------------

#include <physics/physics.h>

//---------------------------------------------------------------------------

namespace asd
{
    using namespace space_literals;

	class plane_object : public space::oriented
	{
	public:
		plane_object(physical_layer & world, space::real level) :
            space::oriented(space::vector{0.0_x, level, 0.0_x}, space::quaternion{ space::vector_constants::up, space::vector_constants::forward }),
			_shape(std::make_unique<btStaticPlaneShape>(btVector3{0.0_x, 1.0_x, 0.0_x}, 0.0_x)),
            _physical(std::make_unique<physical>(*this, world, *_shape))
		{
		}

		virtual ~plane_object() {}

	protected:
		std::unique_ptr<btStaticPlaneShape> _shape;
		std::unique_ptr<physical> _physical;
	};
}

//---------------------------------------------------------------------------
#endif
